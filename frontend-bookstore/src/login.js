console.log("Hello World! I'm Your baby and I'm almost there with you guys :)");
async function loginClick() {
    var inputEmail;
    inputEmail = document.getElementById("email").value;
    window.alert(inputEmail);
    var inputPassword = document.getElementById("password").value;
    var url;
    url = `http://localhost:8080/api/login?login=${inputEmail}&password=${inputPassword}`;
    console.log(url);
    // fetch(url, {method: "post"}).then(n => console.log(n.text()));
    var response = await fetch(url, {method: "post"});
    var message = await response.text();
}

async function loginSubmit(form, event) {
    event.preventDefault();
    var data;
    data = new URLSearchParams(new FormData(form));
    var url;
    url = `http://localhost:8080/api/login`;
    console.log(url);
    // fetch(url, {method: "post"}).then(n => console.log(n.text()));
    var response = await fetch(url, {method: "post",body:data});
    var message = await response.text();
    if (response.status === 200){
        window.location.href = "index";
    }else{
        var windowAlert;
        windowAlert = document.getElementById("alert");
        windowAlert.hidden = false;
        windowAlert.innerText = message;
    }
    console.log(message);
    return false;
}